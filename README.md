# Apple Swap

- Easy to swap the token on Binance Smart Chain
- Status: Developing

## Stack

- ReactJS, Recoil, Typescript, Coingecko API

## Install

```sh
npm install
mv .env.example .env.local
npm start
```
