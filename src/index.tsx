import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { RecoilRoot } from "recoil";
import "./index.css";
import reportWebVitals from "./reportWebVitals";

const LazyHome = React.lazy(() => import("./pages/Home"));

const router = createBrowserRouter([
  {
    path: "/",
    element: <LazyHome />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  // <React.StrictMode>
  <RecoilRoot>
    <Suspense fallback="...">
      <RouterProvider router={router} fallbackElement="..." />
    </Suspense>
  </RecoilRoot>
  // </React.StrictMode>
);

reportWebVitals();
