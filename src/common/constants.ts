import { IChain } from "../interfaces/metamask.interface";
import { User } from "../interfaces/user.interface";
import { TMetamaskError, TModalState } from "./types";

export const NETWORK_CHAIN: IChain[] = [
  {
    chainId: "0x1",
    name: "Ethereum",
    logoURI:
      "https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880",
    symbol: "ETH",
    address: "",
  },
  {
    chainId: "0x38",
    name: "Binance",
    logoURI:
      "https://assets.coingecko.com/coins/images/825/large/bnb-icon2_2x.png?1644979850",
    symbol: "BNB",
    address: "",
  },
];

// Default data

export const DEFAULT_NETWORK = NETWORK_CHAIN[1];

export const METAMASK_ERROR: TMetamaskError = {
  4001: "User rejected the request",
  4100: "The request is for another chain",
  4200: "'The requested method is not supported by this Ethereum provider",
  4004: "The chain is not supported, please switch to Ethereum or Binance",
  4005: "You need connect to MetaMask",
};

export const DEFAULT_ERROR = "Something went wrong, please try again later";

export const DEFAULT_TOAST_OPTIONS = {
  autoClose: 2000,
  hideProgressBar: true,
};

export const DEFAULT_USER: User = {
  address: "",
  balance: BigInt(0),
};

export const DEFAULT_MODAL_STATE: TModalState = {
  isOpen: false,
  type: "inputCurrency",
};

// format number
export const BALANCE_FORMAT_DECIMALS = 5;

export const MAX_SYMBOL_LENGTH = 8;
