export type TMetamaskError = { [key: string]: string };

export type TErrorType = "metamask" | "api" | "unknown";

export type TypeOptions = "info" | "success" | "warning" | "error" | "default";

export type TModalState = {
  isOpen: boolean;
  type: "inputCurrency" | "outputCurrency";
};

export type TBSCScanResponse = {
  status: "0" | "1";
  message: string;
  result: string;
};
