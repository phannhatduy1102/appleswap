import { selector } from "recoil";
import { IToken } from "../interfaces/token.interface";
import { searchToken } from "../utils";
import { listTokenState, searchTokenState } from "./atoms";

const SELECTOR_KEY = {
  filteredListToken: "filteredListToken",
};

const filteredListToken = selector<IToken[]>({
  key: SELECTOR_KEY.filteredListToken,
  get: ({ get }) => {
    const searchValue = get(searchTokenState);
    const listToken = get(listTokenState);
    if (searchValue) {
      return searchToken(listToken, searchValue);
    }
    return listToken;
  },
});

export { filteredListToken };
