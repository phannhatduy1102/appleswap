import { atom } from "recoil";
import { DEFAULT_MODAL_STATE, DEFAULT_NETWORK } from "../common/constants";
import { TModalState } from "../common/types";
import { IChain } from "../interfaces/metamask.interface";
import { IToken } from "../interfaces/token.interface";
import { User } from "../interfaces/user.interface";
import BSCToken from "../json/BSCToken.json";

const ATOM_KEY = {
  network: "network",
  metaMask: "metamask",
  user: "user",
  inputCurrency: "inputCurrency",
  outputCurrency: "outputCurrency",
  listToken: "listToken",
  selectedTokenInModal: "selectedTokenInModal",
  isModalOpen: "isModalOpen",
  searchToken: "searchToken",
  inputCurrencyNumber: "inputCurrencyNumber",
  outputCurrencyNumber: "outputCurrencyNumber",
};

const networkState = atom<IChain>({
  key: ATOM_KEY.network,
  default: DEFAULT_NETWORK,
});

const userState = atom<User>({
  key: ATOM_KEY.user,
  default: {
    address: "",
    balance: BigInt(0),
  },
});

const inputCurrencyState = atom<IToken | undefined>({
  key: ATOM_KEY.inputCurrency,
  default: undefined,
});

const outputCurrencyState = atom<IToken | undefined>({
  key: ATOM_KEY.outputCurrency,
  default: undefined,
});

const inputCurrencyNumberState = atom<number>({
  key: ATOM_KEY.inputCurrencyNumber,
  default: 0,
});

const outputCurrencyNumberState = atom<number>({
  key: ATOM_KEY.outputCurrencyNumber,
  default: 0,
});

const listTokenState = atom<IToken[]>({
  key: ATOM_KEY.listToken,
  default: BSCToken.tokens,
});

const selectedTokenInModalState = atom<IToken>({
  key: ATOM_KEY.selectedTokenInModal,
  default: DEFAULT_NETWORK,
});

const modalState = atom<TModalState>({
  key: ATOM_KEY.isModalOpen,
  default: DEFAULT_MODAL_STATE,
});

const searchTokenState = atom({
  key: ATOM_KEY.searchToken,
  default: "",
});

export {
  networkState,
  userState,
  inputCurrencyState,
  outputCurrencyState,
  listTokenState,
  selectedTokenInModalState,
  modalState,
  searchTokenState,
  inputCurrencyNumberState,
  outputCurrencyNumberState,
};
