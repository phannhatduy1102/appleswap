import { toast } from "react-toastify";
import { Web3Metamask } from "../api/Web3Client";
import {
  DEFAULT_ERROR,
  DEFAULT_TOAST_OPTIONS,
  METAMASK_ERROR,
  NETWORK_CHAIN,
} from "../common/constants";
import { TErrorType, TypeOptions } from "../common/types";
import { IChain } from "../interfaces/metamask.interface";
import { IToken } from "../interfaces/token.interface";

const convertToEther = (value: bigint, unit: any): number => {
  const result = Web3Metamask.utils.fromWei(value, unit);
  return Number(result);
};

const sliceAddress = (address: string): string => {
  return `${address.slice(0, 6)}...${address.slice(-4)}`;
};

const pushToast = (
  notiType: TypeOptions,
  errorType: TErrorType,
  errorCode: number,
  message?: string
): void => {
  const errorMessage = message || getErrorMessage(errorCode);

  switch (errorType) {
    case "metamask":
      toast(message || errorMessage, {
        ...DEFAULT_TOAST_OPTIONS,
        type: notiType,
      });
      break;

    default:
      toast.error(message || DEFAULT_ERROR, DEFAULT_TOAST_OPTIONS);
      break;
  }
};

const getErrorMessage = (errorCode: keyof typeof METAMASK_ERROR): string => {
  return METAMASK_ERROR?.[errorCode] || DEFAULT_ERROR;
};

const findChainData = (chainId: string): IChain | undefined => {
  const chainData = NETWORK_CHAIN.find((chain) => chain.chainId === chainId);
  return chainData;
};

const isChainSupported = (chainId: string): boolean => {
  return !!findChainData(chainId);
};

const searchToken = (tokens: IToken[], searchValue: string) => {
  if (searchValue) {
    const findByAddress = tokens.find((e) => e.address === searchValue);
    if (findByAddress) {
      return [findByAddress];
    }
    return tokens.filter((e) => {
      const parseName = e.name.toLowerCase();
      const parseSymbol = e.symbol.toLowerCase();
      const parseSearchValue = searchValue.toLowerCase();
      if (parseSymbol === parseSearchValue) {
        return e;
      }
      if (parseName.search(parseSearchValue) !== -1) {
        return e;
      }
      return null;
    });
  }
  return tokens;
};

export {
  convertToEther,
  sliceAddress,
  pushToast,
  findChainData,
  isChainSupported,
  searchToken,
};
