import { ChangeEvent } from "react";
import Modal from "react-modal";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { useDebouncedCallback } from "use-debounce";
import {
  modalState,
  searchTokenState,
  selectedTokenInModalState,
} from "../../recoil/atoms";
import ListToken from "./ListToken";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    borderRadius: "16px",
    padding: 0,
  },
};

const ModalSelectToken = () => {
  const [modal, setModalState] = useRecoilState(modalState);
  const selectedToken = useRecoilValue(selectedTokenInModalState);
  const setSearchValue = useSetRecoilState(searchTokenState);

  const debounced = useDebouncedCallback((e: ChangeEvent<HTMLInputElement>) => {
    onSearchChange(e);
  }, 500);

  const onSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setSearchValue(value);
  };

  const onModalClose = () => {
    setModalState({ ...modal, isOpen: false });
    setSearchValue("");
  };

  return (
    <>
      <Modal
        style={customStyles}
        isOpen={modal.isOpen}
        overlayClassName={`fixed inset-0 bg-[#00000066]`}
      >
        <div className={`w-96 max-w-sm z-50 bg-white p-4`}>
          <div className="flex justify-between mb-4 items-center">
            <p className="captialize text-base">Select a token</p>
            <button
              type="button"
              className="text-gray-700  bg-transparent hover:bg-slate-100 rounded-lg text-base p-1.5 ml-auto inline-flex items-center"
              data-modal-toggle="crypto-modal"
              onClick={onModalClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>
          <div className="text-base">
            <input
              type="text"
              className="w-full p-4 outline-none focus:border-secondary border-gray-300 border rounded-xl"
              placeholder="Search name or address"
              onChange={debounced}
            />
          </div>
          {selectedToken && (
            <div>
              <button
                className="child:inline-block border px-2 py-1.5 rounded-lg mt-4 bg-gray2 text-base"
                disabled
              >
                <img
                  src={`${selectedToken.logoURI}`}
                  alt=""
                  className="max-w-[24px] max-h-[24px] mr-2"
                />
                <span>{selectedToken.symbol}</span>
              </button>
            </div>
          )}
        </div>
        <hr className="" />
        <ListToken />
      </Modal>
    </>
  );
};

export default ModalSelectToken;
