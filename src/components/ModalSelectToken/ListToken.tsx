import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { useDebouncedCallback } from "use-debounce";
import { IToken } from "../../interfaces/token.interface";
import {
  inputCurrencyState,
  modalState,
  outputCurrencyState,
} from "../../recoil/atoms";
import { filteredListToken } from "../../recoil/selectors";

const ListToken = () => {
  const tokens = useRecoilValue(filteredListToken);
  const [modal, setModalState] = useRecoilState(modalState);
  const setInputCurrency = useSetRecoilState(inputCurrencyState);
  const setOutputCurrency = useSetRecoilState(outputCurrencyState);
  const debounceSelectToken = useDebouncedCallback((token: IToken) => {
    if (token && modal) {
      if (modal.type === "inputCurrency") {
        setInputCurrency(token);
      } else {
        setOutputCurrency(token);
      }
      setModalState({ ...modal, isOpen: false });
    }
  }, 500);

  return (
    <div>
      <ul className="min-h-[400px] max-h-[400px] overflow-y-auto">
        {tokens.slice(0, 500).map((token) => (
          <li
            key={token.address}
            className="hover:bg-gray1 cursor-pointer"
            onClick={() => {
              debounceSelectToken(token);
            }}
          >
            <div className="flex py-2 items-center px-4">
              <img
                src={`${token.logoURI}`}
                alt=""
                className="w-[28px] h-[28px] mr-3"
              />
              <div className="w-full">
                <p className="font-medium text-base w-[280px] truncate">
                  {token.name}
                </p>
                <p className="font-extralight text-13 text-gray-400">
                  {token.symbol}
                </p>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ListToken;
