import { useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { Web3Api } from "../../api/Web3Client";
import { NETWORK_CHAIN } from "../../common/constants";
import useMetaMask from "../../hooks/useMetaMask";
import { networkState, userState } from "../../recoil/atoms";
import { IChain, ProviderRpcError } from "../../interfaces/metamask.interface";
import { isChainSupported, pushToast } from "../../utils";
import NetworkChain from "./components/NetworkChain";
import UserInfoHorizontal from "./components/UserInfoHorizontal";

const Header = () => {
  const { getChainId, requestSwitchChain, requestAccount } = useMetaMask();

  const chain = useRecoilValue(networkState);
  const [user, setUser] = useRecoilState(userState);
  const [isShowChain, setShowChain] = useState<boolean>(false);

  const onChainChange = () => {
    setShowChain(!isShowChain);
  };

  const handleChainChange = (isChainChange: boolean, chain: IChain) => {
    const { chainId } = chain;
    const { address } = user;
    if (isChainChange && chainId && address) {
      requestSwitchChain(chainId)
        .then()
        .catch((error: ProviderRpcError) => {
          pushToast("error", "metamask", error?.code, error?.message);
        });
    } else {
      pushToast("error", "metamask", 4005);
    }
    setShowChain(false);
  };

  const onLoginClick = async () => {
    try {
      const currentChain = await getChainId();
      if (isChainSupported(currentChain)) {
        const account = await requestAccount();
        const balance = await Web3Api.getBalance(account[0]);
        if (account.length > 0) {
          setUser({ address: account[0], balance });
        }
      } else {
        pushToast("error", "metamask", 4004);
      }
    } catch (error) {}
  };

  return (
    <>
      <div className="flex pt-4 justify-between ">
        <div className="max-w-[32px] col-2">
          <img
            src="https://seeklogo.com/images/U/uniswap-logo-782F5E6363-seeklogo.com.png"
            alt=""
          />
        </div>
        <div className="col-6">
          <div className="text-right">
            <ul className="inline-block space-x-2 child:inline-block child:px-3 child:py-1.5 text-base bg-white rounded-full text-gray-600 last:pr-2">
              <li className="font-medium bg-gray1 rounded-xl ml-0.5">Swap</li>
              <li>Pool</li>
              <li>Vote</li>
              <li>Charts</li>
            </ul>
          </div>
        </div>
        <div className="col-4 justify-end pl-4 relative">
          <div className="flex flex-auto flex-nowrap space-x-2 max-w-full overflow-x-hidden justify-end">
            <button
              onClick={onChainChange}
              className="header-btn child:inline-block cursor-pointer"
            >
              {chain?.chainId && (
                <span className="inline-block mr-2">
                  <span className="inline-block max-w-[24px] max-h-[18px] mr-1">
                    <img src={`${chain?.logoURI}`} alt="" />
                  </span>
                  <span>{chain.name}</span>
                </span>
              )}
              <span
                dangerouslySetInnerHTML={{
                  __html: `<ion-icon name="chevron-down-outline" style="vertical-align : middle"></ion-icon>`,
                }}
              ></span>
            </button>
            <div>
              {!user.address ? (
                <button
                  onClick={onLoginClick}
                  className=" text-secondary header-btn hover:opacity-80"
                >
                  Connect Wallet
                </button>
              ) : (
                <UserInfoHorizontal chain={chain} user={user} />
              )}
            </div>

            {isShowChain && (
              <div className="chain absolute top-11 left-4 w-[60%]">
                <div className="px-4 py-4 bg-white rounded-lg text-15">
                  <p className="mb-4 font-light">Select a network</p>
                  <ul className="child:cursor-pointer child:py-3 child:px-2 flex flex-col">
                    {NETWORK_CHAIN.map((item) => (
                      <NetworkChain
                        data={item}
                        key={item.chainId}
                        isActive={chain.chainId === item.chainId}
                        chainCb={handleChainChange}
                      />
                    ))}
                  </ul>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
