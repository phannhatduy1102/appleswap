import { BALANCE_FORMAT_DECIMALS } from "../../../common/constants";
import { IChain } from "../../../interfaces/metamask.interface";
import { User } from "../../../interfaces/user.interface";
import { convertToEther, sliceAddress } from "../../../utils";

interface IUserInfoProps {
  chain: IChain;
  user: User;
}
const UserInfoHorizontal = ({ chain, user }: IUserInfoProps) => {
  const { address, balance } = user;
  const { symbol } = chain;

  const balanceInEther = convertToEther(balance, "ether").toFixed(
    BALANCE_FORMAT_DECIMALS
  );

  return (
    <div className="flex-grow">
      <div className="flex justify-between header-btn flex-wrap space-x-2">
        <p>{`${balanceInEther} ${symbol}`}</p>
        <div>
          <p className="bg-stone-100 px-2 rounded-lg">
            {sliceAddress(address)}
          </p>
        </div>
      </div>
    </div>
  );
};
export default UserInfoHorizontal;
