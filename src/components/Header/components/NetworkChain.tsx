import { useRecoilState } from "recoil";
import { IChain } from "../../../interfaces/metamask.interface";
import { networkState } from "../../../recoil/atoms";

interface INetworkChain {
  data: IChain;
  isActive?: boolean;
  chainCb: (isChainChange: boolean, data: IChain) => void;
}

const NetworkChain = ({ data, isActive = false, chainCb }: INetworkChain) => {
  const [chain] = useRecoilState(networkState);
  const { chainId, name, logoURI } = data;

  const onChainClick = () => {
    const isChainChange = chain.chainId !== chainId;
    chainCb(isChainChange, data);
  };

  return (
    <li
      className={`${isActive && "is-chain-active order-first"}`}
      onClick={onChainClick}
    >
      <span>
        <span className="inline-block max-w-[24px] max-h-[18px] mr-2">
          <img src={`${logoURI}`} alt="" />
        </span>
        <span>{name}</span>
      </span>
    </li>
  );
};

export default NetworkChain;
