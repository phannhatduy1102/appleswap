import {
  ChangeEvent,
  HTMLProps,
  useCallback,
  useEffect,
  useState,
} from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { useDebouncedCallback } from "use-debounce";
import { TSwapInputType } from ".";
import { Web3Api } from "../../api/Web3Client";
import {
  BALANCE_FORMAT_DECIMALS,
  DEFAULT_NETWORK,
  MAX_SYMBOL_LENGTH,
} from "../../common/constants";
import { IToken } from "../../interfaces/token.interface";
import {
  inputCurrencyNumberState,
  modalState,
  outputCurrencyNumberState,
  selectedTokenInModalState,
  userState,
} from "../../recoil/atoms";
import { convertToEther } from "../../utils";

interface ISwapInputProps extends HTMLProps<HTMLButtonElement> {
  type: TSwapInputType;
  token?: IToken;
}

interface ISelectTokenButtonProps {
  type?: "token" | "default";
  token?: IToken;
  handleSelectToken: (token?: IToken) => void;
}

const SwapInput = ({ className, type, token }: ISwapInputProps) => {
  const [balance, setBalance] = useState<bigint>(BigInt(0));

  const setModalState = useSetRecoilState(modalState);
  const user = useRecoilValue(userState);
  const setSelectedToken = useSetRecoilState(selectedTokenInModalState);
  const setInputCurrencyNumber = useSetRecoilState(inputCurrencyNumberState);
  const setOutputCurrencyNumber = useSetRecoilState(outputCurrencyNumberState);

  useEffect(() => {
    const initial = async () => {
      if (token && user && type === "inputCurrency") {
        if (token.symbol === DEFAULT_NETWORK.symbol) {
          const balance = await Web3Api.getBalance(user.address);
          setBalance(balance);
        } else {
          const balance = await Web3Api.getBalanceByContract(
            user.address,
            token?.address
          );
          setBalance(balance);
        }
      }
    };
    initial();
  }, [token, user, type]);

  const onSelecTokenClick = (token?: IToken) => {
    if (token) {
      setSelectedToken(token);
    }
    setModalState({ type, isOpen: true });
  };

  const debouncedOnChange = useDebouncedCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;
      if (type === "inputCurrency") {
        setInputCurrencyNumber(Number(value) || 0);
      }
      if (type === "outputCurrency") {
        setOutputCurrencyNumber(Number(value) || 0);
      }
    },
    500
  );

  return (
    <div
      className={`p-4 bg-gray-50 rounded-2xl border border-transparent hover:border-gray-200 ${className}`}
    >
      <div className="flex justify-between">
        <input
          type="number"
          className="text-28 bg-gray-50 outline-none col-8 pr-3 text-gray-700"
          min={0}
          placeholder="0.0"
          onChange={debouncedOnChange}
        />
        <div className="col-4 text-right">
          {token ? (
            <SelectTokenButton
              type="token"
              token={token}
              handleSelectToken={onSelecTokenClick}
            />
          ) : (
            <SelectTokenButton handleSelectToken={onSelecTokenClick} />
          )}
        </div>
      </div>
      <div
        className={`text-right mt-2 text-gray-500 ${
          token && type === "inputCurrency" ? "visible" : "hidden"
        }`}
      >
        <p>{`Balance: ${convertToEther(balance, "ether").toFixed(
          BALANCE_FORMAT_DECIMALS
        )}`}</p>
      </div>
    </div>
  );
};

const SelectTokenButton = ({
  type = "default",
  token,
  handleSelectToken,
}: ISelectTokenButtonProps) => {
  const classByType =
    type === "default"
      ? "text-white bg-secondary hover:bg-gray-300 hover:text-white text-15"
      : "text-black bg-gray2 text-base hover:bg-secondary hover:text-white";

  const sliceSymbol = token?.symbol?.slice(0, MAX_SYMBOL_LENGTH) || "";

  const cbSelectToken = useCallback(() => {
    if (handleSelectToken) {
      handleSelectToken(token);
    }
  }, [handleSelectToken, token]);

  return (
    <button
      className={`capitalize px-3 py-1.5 rounded-2xl inline-block  ${classByType}`}
      onClick={cbSelectToken}
    >
      <div className="flex space-x-1">
        {type === "default" ? (
          <p>Select token</p>
        ) : (
          <>
            <img src={`${token?.logoURI}`} alt="" className="max-w-[24px]" />
            <span className="inline-block">{sliceSymbol}</span>
          </>
        )}
        <p
          dangerouslySetInnerHTML={{
            __html: `<ion-icon name="chevron-down-outline" style="vertical-align : middle"></ion-icon>`,
          }}
        ></p>
      </div>
    </button>
  );
};

export default SwapInput;
