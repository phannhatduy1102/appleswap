import { useRecoilState, useRecoilValue } from "recoil";
import SwapInput from "./SwapInput";
import {
  inputCurrencyNumberState,
  inputCurrencyState,
  outputCurrencyNumberState,
  outputCurrencyState,
} from "../../recoil/atoms";
import { useEffect } from "react";
import { Web3JSAPI } from "../../api/web3js";

export type TSwapInputType = "inputCurrency" | "outputCurrency";

const Swap = () => {
  const inputCurrency = useRecoilValue(inputCurrencyState);
  const outputCurrency = useRecoilValue(outputCurrencyState);

  const [inputNumber] = useRecoilState(inputCurrencyNumberState);
  const [outputNumber] = useRecoilState(outputCurrencyNumberState);

  useEffect(() => {
    Web3JSAPI.getAmountOutput(
      "100",
      "0xe9e7cea3dedca5984780bafc599bd69add087d56",
      "0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82"
    );
  }, [inputNumber]);

  useEffect(() => {}, [outputNumber]);

  return (
    <div className="max-w-[480px] shadow-lg rounded-3xl bg-white mx-auto mt-20">
      <div className="w-full px-3 py-4">
        <p className="text-base ml-4 mb-4">Swap</p>
        <div>
          <SwapInput
            className="mb-3"
            type="inputCurrency"
            token={inputCurrency}
          />
          <SwapInput type="outputCurrency" token={outputCurrency} />
        </div>
      </div>
    </div>
  );
};

export default Swap;
