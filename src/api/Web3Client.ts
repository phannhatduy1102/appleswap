import Web3 from "web3";
import { TBSCScanResponse } from "../common/types";
import { IWeb3Api } from "../interfaces/web3js.interface";
import { pushToast } from "../utils";

const Web3Metamask: Web3 = new Web3(window?.ethereum as any);
const Web3Client: Web3 = new Web3("https://bsc-dataseed.binance.org/");

// const RouterV2Contract = new Web3Client.eth.Contract(
//   test as AbiItem[],
//   `${process.env.PANCAKE_ROUTER_CONTRACT}`
// );

const Web3Api: IWeb3Api = {
  getBalance: async (address) => {
    if (!address) return BigInt(0);
    const balance = await Web3Metamask.eth.getBalance(address);
    return balance;
  },
  getBalanceByContract: async (address, contract) => {
    if (!address) return BigInt(0);
    const balance = await fetch(
      `https://api.bscscan.com/api?module=account&action=tokenbalance&contractaddress=${contract}&address=${address}&tag=latest&apikey=${process.env.BSCSCAN_KEY}`
    );
    const result: TBSCScanResponse = await balance.json();
    if (result.status === "0") {
      pushToast("error", "api", 0, result.result);
      return BigInt(0);
    }
    return BigInt(result.result);
  },
  // getAmountOutput: async (amountIn, path) => {
  //   const amountOut = await RouterV2Contract.methods
  //     .getAmountOut(amountIn, path)
  //     .encodeABI();

  //   console.log(amountOut);
  //   return "";
  // },
};

export { Web3Metamask, Web3Api, Web3Client };
