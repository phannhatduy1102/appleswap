import Web3 from "web3";
import BSCRouterV2ABI from "../abis/BSCRouterV2ABI";

const Web3Client = new Web3("https://bsc-dataseed.binance.org/");

const RouterV2Contract = new Web3Client.eth.Contract(
  BSCRouterV2ABI,
  `${process.env.REACT_APP_PANCAKE_ROUTER_CONTRACT}`
);

const Web3JSAPI = {
  getAmountOutput: async (amountIn, fromToken, toToken) => {
    const amountOut = await RouterV2Contract.methods
      .getAmountOut(amountIn, fromToken, toToken)
      .call();
    console.log(amountOut);
    return "";
  },
};

export { Web3JSAPI };
