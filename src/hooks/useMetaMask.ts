// eslint-disable-next-line import/no-anonymous-default-export
import detectEthereumProvider from "@metamask/detect-provider";
import { useEffect, useState } from "react";
import { Web3Api, Web3Metamask } from "../api/Web3Client";
import { User } from "../interfaces/user.interface";

const useMetaMask = () => {
  const [provider, setProvider] = useState<any>(null);

  useEffect(() => {
    const init = async () => {
      const provider: any = await detectEthereumProvider();
      if (window?.ethereum && provider) {
        setProvider(provider);
      }
    };
    init();
  }, []);

  const getChainId = async (): Promise<string> => {
    const chainId = await provider?.request({ method: "eth_chainId" });
    return Web3Metamask.utils.toHex(chainId);
  };

  const getAccount = () => {
    try {
      return provider?.request({
        method: "eth_accounts",
      });
    } catch (error) {
      throw error;
    }
  };

  const getUser = async (): Promise<User> => {
    const account = await getAccount();
    const balance = await Web3Api.getBalance(account[0]);
    return {
      address: account[0],
      balance,
    };
  };

  const requestAccount = (): Promise<string[]> => {
    try {
      return provider?.request({
        method: "eth_requestAccounts",
      });
    } catch (error) {
      throw error;
    }
  };

  const requestSwitchChain = async (chainId: string): Promise<void> => {
    const isConnected: boolean = provider?.isConnected();
    if (isConnected) {
      try {
        await provider?.request({
          method: "wallet_switchEthereumChain",
          params: [{ chainId }],
        });
      } catch (error) {
        throw error;
      }
    }
  };

  return {
    provider,
    getChainId,
    getAccount,
    getUser,
    requestAccount,
    requestSwitchChain,
  };
};

export default useMetaMask;
