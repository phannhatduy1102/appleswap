import React, { useEffect } from "react";
import Modal from "react-modal";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSetRecoilState } from "recoil";
import { Web3Api, Web3Metamask } from "../api/Web3Client";
import { DEFAULT_USER } from "../common/constants";
import useMetaMask from "../hooks/useMetaMask";
import { IChain } from "../interfaces/metamask.interface";
import { inputCurrencyState, networkState, userState } from "../recoil/atoms";
import { findChainData, pushToast } from "../utils";

const Header = React.lazy(() => import("../components/Header"));
const SwapComponent = React.lazy(() => import("../components/Swap"));
const ModalSelectToken = React.lazy(
  () => import("../components/ModalSelectToken")
);

Modal.setAppElement("#root");

export const Home = () => {
  const { provider, getAccount, getChainId, getUser } = useMetaMask();
  const setUser = useSetRecoilState(userState);
  const setNetWork = useSetRecoilState(networkState);
  const setInputCurrency = useSetRecoilState(inputCurrencyState);

  useEffect(() => {
    console.log();

    if (provider) {
      Web3Metamask.setProvider(provider);
      provider.on("chainChanged", (chainId: string) => {
        const chainData: IChain | undefined = findChainData(chainId);
        if (!chainData) {
          pushToast("error", "metamask", 4004);
          setUser(DEFAULT_USER);
        } else {
          getUser().then((user) => {
            setUser(user);
          });
          setNetWork(chainData);
        }
        setInputCurrency(chainData);
      });
      provider.on("accountsChanged", (accounts: string[]) => {
        if (accounts.length === 0) {
          setUser({ address: "", balance: BigInt(0) });
        }
      });
      const initial = async () => {
        const account = await getAccount();
        const isConnected = account.length > 0 ? true : false;
        const chainId = await getChainId();

        if (isConnected && chainId) {
          const balance = await Web3Api.getBalance(account[0]);
          const chainData: IChain | undefined = findChainData(chainId);
          if (!chainData) {
            pushToast("error", "metamask", 4004);
          } else {
            setNetWork(chainData);
            setUser({ address: account[0], balance: balance });
            setInputCurrency(chainData);
          }
        }
      };
      initial();
    }
  }, [
    provider,
    getUser,
    setUser,
    setNetWork,
    getAccount,
    getChainId,
    setInputCurrency,
  ]);

  return (
    <>
      <div className="bg-primary min-h-screen">
        <div className="2xl:container mx-auto px-4">
          <Header />
          <SwapComponent />
        </div>
      </div>
      <ToastContainer />
      <ModalSelectToken />
    </>
  );
};

export default Home;
