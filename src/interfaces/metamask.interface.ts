import { IToken } from "./token.interface";

interface ProviderRpcError extends Error {
  message: string;
  code: number;
  data?: unknown;
}

interface ConnectInfo {
  chainId: string;
}

interface IChain extends IToken {}

export type { ProviderRpcError, ConnectInfo, IChain };
