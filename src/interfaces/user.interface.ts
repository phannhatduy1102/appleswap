export interface User {
  address: string;
  balance: bigint;
}
