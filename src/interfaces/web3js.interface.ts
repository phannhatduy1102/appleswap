interface IWeb3Api {
  getBalance: (address: string) => Promise<bigint>;
  getBalanceByContract: (address: string, contract: string) => Promise<bigint>;
  //getAmountOutput: (amountIn: string, path: string[]) => Promise<string>;
}

export type { IWeb3Api };
