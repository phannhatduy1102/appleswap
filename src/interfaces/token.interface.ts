interface IToken {
  chainId: string;
  name: string;
  address: string;
  symbol: string;
  logoURI?: string;
}

export type { IToken };
